# Training Materials

### IDOR

[PTAX-1-A, Introduction to Residential Assessment Practices](https://www2.illinois.gov/rev/localgovernments/property/Documents/ptax-1-a.pdf)
 - Unit 1 | An Overview of the Property Tax Cycle and the Appeal Process (p. 11-26)

[PTAX-1-B, Introduction to Commercial Assessments](https://www2.illinois.gov/rev/localgovernments/property/Documents/ptax-1-b.pdf)
 - Unit 1 | Appraisal Theory (p. 11-20)


[PTAX-1-BR, Board of Review-Basic Course](https://www2.illinois.gov/rev/localgovernments/property/Documents/ptax-1-br.pdf)
 - Unit 1 | Duties, Responsibilities and Procedures (p. 17-27, 98)
 - Unit 2 | An Overview of the Property Tax Cycle (p. 101-116)
 - Unit 7 | Levy (p. 177-180)
 - Unit 11 | PTAB: Appeals at the Illinois Property Tax Appeal Board (p. 221-225)

 [PTAX-1-E, Introduction to Sales Ratio Studies](https://www2.illinois.gov/rev/localgovernments/property/Documents/ptax-1-e.pdf)
  - Unit 2 | PTAX-203 Form/The Sales Ratio Study (p. 18-25, 31-32)
  - Unit 3 | Measure for the Uniformity of Assessments

### IAAO
 
_The material listed here is not available for free online. Register for classes [here](https://learn.iaao.org/catalog#form_type=catalog-quick-filter&page=1&webinar_type=on-demand&sort_by=title_ascending)._

Self Study 101 | [Fundamentals of Real Property Appraisal](https://www.iaao.org/media/pro_dev/course_information_packets/Course_101_Info.pdf) ~ 30 hours based on live class length
> The Fundamentals of Real Property Appraisal is designed to provide the students with an understanding and working knowledge of the procedures and techniques required to estimate the market value of vacant and improved properties. This course concentrates on the skills necessary for estimating the market value of properties using two approaches to value: the cost approach and the sales comparison approach.

_this course may be redundant given what's available for free through IDOR, but IAAO seems to provide a more robust and in-depth learning experience._

Self Study 300 | [Fundamentals of Mass Appraisal](https://www.iaao.org/media/pro_dev/course_information_packets/Course_300_info.pdf) ~ 30 hours based on live class length
> This course introduces students to mass appraisal and is a prerequisite for the IAAO 300-level course series. Topics covered include single-property appraisal as it compares to mass appraisal, components of a mass appraisal system, data requirements and analysis, introduction to statistics, use of assessment ratio studies in mass appraisal, modeling of the three approaches to value, and selection of a mass appraisal system.
