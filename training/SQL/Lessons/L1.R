### CCAO SQL Training
### Lecture 1 - Introduction and SQL Concepts

# This series is designed to prepare Jr. and Sr. modeling analysts to effectivly use the
# CCAO's data warehouse.

# This lesson coveres the basics of SQL within the R environment

library(sqldf)

# First, we will create a data frame to use in our examples.
# The first column of this data frame will be numeric: 1,2,3
# The second column is character: A, B, C
df1 <- data.frame(
   id=c(11,21,31)
  , x=c(1,2,3)
  , y=c('A', 'B', 'C')
  )

df1

# Notice that this data frame is unique by ID. It is always important to understand
# your data and what identifies each row before you do anything else

# ----SELECT-----

# The SELECT .. FROM... combination identifies what fields, or columns you want to take from your data
# SELECT * FROM... will simply take all the columns from the data

sqldf('SELECT * FROM df1')

# In most cases, you will only want a few columns

sqldf('SELECT id, x FROM df1')

# Often, you will want to change the names of columns you select.
# SELECT x AS [A Number] FROM... will rename the field x to 'A Number' when you ingest it.

sqldf('SELECT id, x AS [A Number] FROM df1')

# -----WHERE----

# Where limits the rows you are ingesting. Suppose we only want observations where
# y is either A or B, but not C

sqldf("SELECT * FROM df1
      WHERE y IN ('A','B')")

# Note that you do not need to select y to condition on y

# You can have many conditions. Use AND and OR to combine conditions

sqldf("SELECT * FROM df1
      WHERE y IN ('A','B')
      AND x>1")

sqldf("SELECT * FROM df1
      WHERE y IN ('A','B')
      OR x=3")

# You can use parenthases to make more complicated conditions

sqldf("SELECT * FROM df1
      WHERE
      (y IN ('A','B') AND x>1)
      OR
      (y='C' AND x>2)")


# ---- DISTINCT ----

df1 <- data.frame(
  id=c(11,21,31, 11,21,31)
  , x=c(1,2,3, 4, 5, 6)
  , y=c('A', 'B', 'C', 'A', 'A', 'C')
)

# The DISTINCT operator modifies the select statement to remove duplicate rows in terms
# of all the fields selected

# The mock data frame above has two sets of observations for each id.

# Suppose I want to know each id <> group combination

sqldf("SELECT DISTINCT id, y FROM df1")

# Notice that id 21 is included twice because it was in two different groups.

# ---- ORDER BY ----
# This command orders the output data.

sqldf("SELECT DISTINCT id, y FROM df1
      ORDER BY id")

# now it is easier to see that 21 appears twice. Include DESC and ASC to get
# descending and ascending order

sqldf("SELECT DISTINCT id, y FROM df1
      ORDER BY id ASC")

sqldf("SELECT DISTINCT id, y FROM df1
      ORDER BY id DESC")

