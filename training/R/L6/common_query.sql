
SELECT SALES.*, TOWN, CLASS, 1 AS n 
FROM VW_CLEAN_IDORSALES SALES
LEFT JOIN (

  SELECT RIGHT('0' + CAST(HD_PIN AS VARCHAR), 14) AS PIN,
    TAX_YEAR, LEFT(HD_TOWN, 2) AS TOWN,
    CASE WHEN HD_CLASS IN (313, 314, 315) THEN 'COMMERCIAL APT'
    	WHEN HD_CLASS = 593 THEN 'INDUSTRIAL BUILDING'
      WHEN HD_CLASS IN (202, 203, 204, 205, 206, 207, 208, 209, 210, 234, 278, 295) THEN 'SF'
      WHEN HD_CLASS IN (211, 212) THEN 'MF'
      WHEN HD_CLASS IN (200, 201, 241) OR (HD_CLASS = 299) 
        AND ((HD_PRI_BLD + HD_PRI_LND) > 10 OR (HD_PRI_BLD + HD_PRI_LND) IS NULL) THEN 'NCHARS'
    	ELSE NULL END AS CLASS FROM AS_HEADTB) HEAD
    	
ON SALES.PIN = HEAD.PIN AND SALES.TAX_YEAR = HEAD.TAX_YEAR

WHERE TOWN IN ({towns})
AND CLASS IS NOT NULL
AND SALES.TAX_YEAR >= {year} - 6
AND sale_price > 10000
