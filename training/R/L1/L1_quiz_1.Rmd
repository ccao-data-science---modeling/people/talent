---
title: "CCAO Programming in R - Quiz 1"
output: pdf_document
---

## Structure

This short quiz is designed to test your knowledge of the material covered in the first days of our programming in R training. Please complete the problem 

## Your Task

Using the same R script you used for the earlier exercises, write your answers as code, then email me the .R script file at DSnow@cookcountyassessor.com

If necessary, you should also include comments that explain what your code is doing or what you are attempting to do. 



# Quiz 1

1. Using `c()` create a variable named `quiz_list` that contains the strings "CCAO", "residential", and "12/3/2019".

2. Using `as.Date()` and square bracket subsetting, convert the third value of `quiz_list` into a date and save it to a variable named `today`.

3. Using `as.Date()` and subtraction, calculate the difference between `today` and January 1st, 1998.

