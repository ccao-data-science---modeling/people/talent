---
title: "CCAO Programming in R - Lecture 3 Exercises"
output: pdf_document
---

# Lecture 3 - Practice Questions

3.1. Write out the code to load the following libraries: `tidyverse`, `stringr`, and `lubridate`.  

3.2. Using `read_csv()`, load the towncode and cert date CSVs into separate dataframes.

3.3. Using a join function, merge the two dataframes into a single dataframe called `merged`.

3.4. Using a filtering join function, keep only observations in `merged` from the *City* township.

3.5. Using `spread()`, create a dataframe named `merged_wide` that contains each year as its own column, with the last appeal date as the values of that column.

3.6. Using `separate()`, create a dataframe called `merged_date_sep` that contains a column for the individual date parts of the *last_appeals_date_accepted* column.

3.7. Use `str_pad()` to zero-pad each date part column of `merged_date_sep`, then save back to `merged_date_sep`.

3.8. Use the `wday()` function from `lubridate` to find the weekday of the last appeal date. What weekday is most common?

3.9. CHALLENGE: Using `lubridate` functions, calculate which township has the most variance in appeal end date.

3.10. CHALLENGE: Using the `forcats` library, convert the *township_name* column in `towncodes` to a factor with the following order: North, City, South.


