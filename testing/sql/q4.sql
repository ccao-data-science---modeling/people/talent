--- QUESTION 1

--- Retrieve PINs and sale prices assigned randomly to years 2019/2020 and
--- classes 200/201

SELECT
  pin,
  random(2) + 2019 AS year,
  random(2) + 200 AS class,
  Cast(sale_price AS varchar) AS sale_price
FROM default.vw_pin_sale limit 10