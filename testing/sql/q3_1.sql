--- QUESTION 4.1

--- Retrieve PINs and classes for 2020

SELECT
    parid as pin,
    taxyr as year,
    class
FROM iasworld.pardat
WHERE
    parid IN (
        '26184070560000',
        '25313120040000',
        '29023120280000',
        '26184070650000',
        '28162070030000',
        '25312150440000',
        '25312150350000',
        '26201020590000',
        '26201090250000',
        '28162070210000'
    ) AND
    taxyr = '2020'