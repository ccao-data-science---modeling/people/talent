--- QUESTION 2

--- Retrieve PINs and uncleaned addresses

SELECT
    parid as pin,
    adrpre,
    adrno,
    adrdir,
    adrstr,
    adrsuf,
    unitdesc,
    unitno
FROM iasworld.pardat limit 6