--- QUESTION 4.2

--- Retrieve PINs and taxdists for 2019, 2020

SELECT
    parid as pin,
    taxyr as year,
    Cast(10000 + random() * 89999 AS int) AS taxdist
FROM iasworld.legdat
WHERE
    parid IN (
        '26184070560000',
        '25313120040000',
        '29023120280000',
        '26184070650000',
        '28162070030000',
        '25312150440000',
        '25312150350000',
        '26201020590000',
        '26201090250000',
        '28162070210000'
    ) AND
    taxyr IN ('2019', '2020')