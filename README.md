### :warning: Development for this project has moved to a [new repository on GitHub](https://github.com/ccao-data/people) :warning:

# Towards a Civil Service in Data Science

How does a local government agency build and maintain a highly-skilled technical team? Any organization can acquire talent in two ways: buy it and/or grow it. Each approach has advantages and disadvantages, and every organization deploys a strategy that mixes acquisition and development in different proportions. 

Local government faces unique challenges when acquiring and retaining technical staff. At the CCAO, these challenges include:

- Employment of data scientists who are party to a collective bargaining agreement.
- Employment processes that are monitored by a federal hiring monitor.
- Budgetary constraints that limit compensation.
- A grade and step pay scale that also limits flexibility in compensation and job titles. 
- A performance review system that does not hew to the nature of data science work.

In the 2011 movie [Moneyball](https://en.wikipedia.org/wiki/Moneyball_(film)), BIlly Beane [says](https://youtu.be/aNDj-H1jxV0?t=140), "If we try to play like the Yankees in here, we will lose to the Yankees out there." Local government cannot adopt the talent acquisition strategies used in the private sector and expect to recruit top talent, as the private sector will almost always out-bid the public sector. A new approach is needed.

## Don't Buy It, Grow It

The CCAO's approach to building and maintaining a highly-skilled and effective data science staff must rely heavily on internal human capital development. Developing human capital is time consuming and requires substantial internal planning and organization. This repository will be used to develop and store training materials for data science staff, as well as define internal training programs and regimens. 

In addition, CCAO leadership must make time and space for human capital development. Leadership must recognize the need for training time and resources, and must limit *work in progress* in order to allow for human capital development. 

### Building Skill Pipelines

The Data Science Department has established two career tracks: technical and leadership. Each track plays a role in developing human capital for the organization. The technical track produces experts in the technology of data science. The leadership track produces experts in the management and strategy of data science. Both sets of expertise are required for a high-functioning data science unit.

The images below display these tracks:

<img src="pipeline/images/leadership-track.png"
     alt="Leadership Track"
     width="500" height="250"
     style="float: left; margin-right: 10px;" />

<img src="pipeline/images/technical-track.png"
     alt="Technical Track"
     width="500" height="250"
     style="float: right; margin-right: 10px;" />

The table below outlines how a technical track staff member might spend their time over their career. It is important to see that, for the first year of employment, the majority of a staff person's time will be spent **learning** rather than **doing**. This investment in human capital will pay dividends to the agency over that staff person's career. 

| Title                         | Tenure       | Structured Learning | Learning From Others | Experience |
|-------------------------------|--------------|---------------------|----------------------|------------|
| Jr. Data Scientist (Training) | 0-3 months   | 50%                 | 30%                  | 20%        |
| Jr. Data Scientist (Training) | 3-6 months   | 40%                 | 40%                  | 20%        |
| Jr. Data Scientist (Training) | 6-9 months   | 40%                 | 40%                  | 20%        |
| Jr. Data Scientist (Training) | 9-12 months  | 30%                 | 30%                  | 40%        |
| Jr. Data Scientist (Training) | 12-18 months | 20%                 | 30%                  | 50%        |
| Jr. Data Scientist (Training) | 18-24 months | 10%                 | 30%                  | 60%        |
| Jr. Data Scientist            | 2-3 years    | 20%                 | 40%                  | 40%        |
| Jr. Data Scientist            | 3-4 years    | 10%                 | 20%                  | 70%        |
| Data Scientist                | 4-6 years    | 10%                 | 20%                  | 70%        |
| Senior Data Scientist         | > 6 years    | 10%                 | 20%                  | 70%        |

## Planning for Attrition: In, Up, and Out

Attrition is common in all industries and sectors, and it is important to build career pipelines that anticipate attrition. At the CCAO, changes in the elected leadership will likely result in attrition of Shakman-exempt positions. Elections happen every four years, so attrition at the top of each leadership pipeline may be quite high. 

If the pipeline outlined above works correctly, a new administration would have highly-capable data scientists and data analysts within the civil service that could be promoted into leadership positions (Sr. Data Scientist, Director of Data Analytics, CDO). Their promotion would trigger a movement of all data science staff up their respective pipelines. By planning for attrition at the top, we can produce a workplace that invests in human capital and promotes from within.

In many cases, high attrition can signal the success of the internal training programs. To understand how that may be, consider military service. A West Point graduate [costs the U.S. Government](https://www.nbcnews.com/id/wbna3072945) about $360,000. In exchange for that investment, the graduate is required to serve for a certain amount of time in the U.S. military. At the end of their service period, they may choose to exit the armed forces and join the private sector. Since officer training at West Point provides valuable skills, many officers do choose to leave military and seek private employment. 

Similarly, if the data science training program successfully produces staff with skills equivalent to their better-paid private sector counterparts, those staff will eventually leave for higher-paying positions. This attrition will be *the best* signal of the training program's efficacy. The talent acquisition and development strategy in the Data Science Department should ultimately plan for a steady flow of people entering, being trained, working for a few additional years, and then leaving. We call this the in-up-out model.

### In: Hire Mission-Driven Staff

The first step in populating the leadership pipeline is to hire entry-level staff. The hiring strategy for these staff is *not* primarily skills-based, since the CCAO will be out-bid by the private sector for technical skills. Instead, the entry-level hiring strategy should be *mission*-driven. In hiring for entry-level staff, the Data Science Department looks for:

- A sound theoretical foundation of mathematics and statistics.
- A commitment to public service in general.
- A commitment to the Data Science Department's [mission and values](https://gitlab.com/groups/ccao-data-science---modeling/-/wikis/Handbook/Mission%20Vision%20Values).

With these three foundational attributes, any new-hire can begin their training and career in the technical or leadership tracks in the Data Science Department. 

### Up: Developing Human Capital

Developing skills is costly. In order to maximize return on investment, the Data Science Department has limited the number of skills staff are expected to develop. We accomplished this by restricting the methods, coding languages, and technologies that we employ in our work. This ensures our work is maintainable in the long-run and gives new hires a clear understanding of the skills they need to develop.

## Core Competencies

As a unit, the Data Science Department focuses on seven core competencies. Each competency has five defined levels corresponding to increasingly difficult technical tasks. The chart below shows the minimum skill levels in each competency required for each position in the Data Science Department. 

![](pipeline/images/core-competencies.png)

In the sections below, we provide examples of tasks for each core competency. 

### SQL

[Training Materials](training/SQL)

* Create custom queries to load data into R and conduct analysis.
* Create views for regularly used data.
* Create ETLs that use SQL to move data between systems.
* Optimize queries for use in interactive applications.
* Write queries to produce bespoke data for ad hoc requests.

### R

[Training Materials](training/R)

* Write scripts to perform ad hoc data analysis.
* Write and modify systems of scripts that drive our CAMA systems.
* Export data from R to Excel for stakeholders.
* Debug scripts that other people have written.
* Use R to conduct statistical analysis and predict property values.

### Shiny

* Understand the basics of reactive programming in Shiny, be able to create very simple applications.
* Understand the high-level basics of webpage structure/languages (HTML, CSS, JavaScript).
* Be able to debug minor issues in Shiny applications.

### Excel

* Create well-formatted tables for consumption by non-data specialists.
* Use pivot tables to summarize data.
* Use VLOOKUP and other matching functions to link data across multiple pages.
* Use macros to mass-produce well-formatted sheets.

### Markdown

* Use multiple R scripts in conjunction with R Markdown to create reproducible reports and memos.
* Use ggplot2 and other data visualization packages to illustrate memos.
* Use markdown documents to create [documentation of work](https://gitlab.com/ccao-data-science---modeling/documentation/wiki_content).

### Git

* Clone git repositories from GitLab using SSH key authentication.
* Use basic git commands (add, commit, push, pull) to contribute to CCAO projects.
* Be able to reconcile failed merges, as well as undo git changes (merge, reset, checkout)
* Use [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) to contribute to projects with multiple branches.

### Leadership

* Understand the employee handbook and all associated protocols.
* Understand the policy implications of data and technological work.
* Be able to associate day-to-day work with overall institutional mission and goals.
* Work across technological and institutional silos to identify high-value projects.

